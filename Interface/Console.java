package Interface;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;
import java.util.Scanner;

import Graphe.Graphe;
import Graphe.Matrice;

public class Console {
	
	private Graphe grapheCourant;
	private Scanner sc;
	static final String DEFAULT_GRAPH_PATH = "src/Data/default.csv";

	public Console() {
		this.sc=new Scanner(System.in);
		this.grapheCourant=new Graphe(DEFAULT_GRAPH_PATH);
		menu();
	}
	
	public void menu() {
		int entree;
		do {
			System.out.println("1  - Afficher le graphe courant");
			System.out.println("2  - Créer un graphe");
			System.out.println("3  - Ouvrir un graphe à partir de fichier");
			System.out.println("4  - Sauvegarder le graphe dans un fichier");
			System.out.println("5  - Ajouter un arc");
			System.out.println("6  - Supprimer un arc");
			System.out.println("7  - Ajouter un sommet");
			System.out.println("8  - Supprimer un sommet");
			System.out.println("9  - Chemin le plus court");
			System.out.println("10 - Rang");
			System.out.println("11 - Ordonnancement");
			System.out.println("12 - kruskal");
			System.out.println("13 - pruffer");
			System.out.println("14 - tarjan");
			System.out.println("0  - Quitter");
			System.out.println("Choisissez une action à réaliser :");
			
			entree = sc.nextInt();
			switch(entree) {
			  case 1:
				afficherGraphe();
			    break;
			  case 2:
				creerGraphe();
			    break;
			  case 3:
				ouvrirGraphe();
			    break;
			  case 4:
				sauvegarderGraphe();
			    break;
			  case 5:
				ajouterArc();
			    break;
			  case 6:
				supprimerArc();
			    break;
			  case 7:
				ajouterSommets();
			    break;
			  case 8:
				supprimerSommet();
			    break;
			  case 9:
		        dijkstra();
			    break;
			  case 10:
				rang();
			    break;
			  case 11:
				ordonnancement();
			    break;
			  case 12:
				kruskal();
				break;
			  case 13:
				pruffer();
				break;
			  case 14:
				tarjan();
				break;

			  default:
			    // code block
			}
			//entree=0;

		}while(entree!=0);

	}
	
	
	private void ordonnancement() {
		ArrayList<Map<String,Double>> res = this.grapheCourant.ordonnancement();
		
		if(res!=null) {
			System.out.println(" Nom de la tâche | Date au plus tôt | Date au plus tard");
			for(int i=0;i<res.size();i++) {
				System.out.println(this.grapheCourant.getSommets().get(i).getNom()+" | "+ res.get(i).get("tot")+" | "+ res.get(i).get("tard"));
			}			
			
		}else {
			System.out.println("Le graphe courant n'est pas éligible à l'algo d'ordonnancement");
		}

	}

	private void rang() {
		ArrayList<Integer> res =this.grapheCourant.rang();
		System.out.println(res.toString());
		
	}
	private void kruskal() {
		Graphe g = new Graphe("Arbre couvrant minimal ",this.grapheCourant.isOriente(), this.grapheCourant.isValue() );
		g.setStruct(this.grapheCourant.kruskal());
		this.grapheCourant.setStruct(this.grapheCourant.kruskal()); 
		
		
	}
	private void pruffer() {
		ArrayList<Integer> res =this.grapheCourant.pruffer();
		System.out.println("Codage de pruffer :"+ res.toString());
		
	}
	private void tarjan() {
		this.grapheCourant =this.grapheCourant.tarjan();
		//System.out.println(res.toString());
		
	}
	

	private void dijkstra() {
		System.out.println("Numéro du sommet de départ");
		int debut = sc.nextInt();
		System.out.println("Numéro du sommet d'arrivée");
		int fin = sc.nextInt();
		ArrayList<Integer> res =this.grapheCourant.Dijkstra(debut,fin);
		System.out.println("Chemin le plus court entre "+this.grapheCourant.getSommets().get(debut).getNom()+" et "+this.grapheCourant.getSommets().get(fin).getNom());
		double distancetotal=0;
		for(int i=0;i<res.size()-1;i++) {
			System.out.print(this.grapheCourant.getSommets().get(res.get(i)).getNom()+"->");
			System.out.print(this.grapheCourant.getStruct().getArc(res.get(i),res.get(i+1))+"->");
			distancetotal+=this.grapheCourant.getStruct().getArc(res.get(i),res.get(i+1));
		}
		System.out.println(this.grapheCourant.getSommets().get(res.get(res.size()-1)).getNom());
		System.out.println("Total du chemin parcourus : "+distancetotal);
	}

	private void supprimerSommet() {
		System.out.println("Numéro du sommet à supprimer");
		int num = sc.nextInt();
		this.grapheCourant.deleteSommet(num);
	}

	private void ajouterSommets() {
		System.out.println("Nom du nouveau sommets");
		sc.nextLine();
		String nom =sc.nextLine();
		this.grapheCourant.addSommet(nom);
		
	}

	private void supprimerArc() {
		System.out.println("Numéro du sommet A");
		int pred = sc.nextInt();
		System.out.println("Numéro du sommet B");
		int suc = sc.nextInt();
		this.grapheCourant.getStruct().deleteSuccesseur(pred, suc);
		if(!this.grapheCourant.isOriente()) {
			this.grapheCourant.getStruct().deleteSuccesseur(suc, pred);
		}
	}

	private void sauvegarderGraphe() {
		System.out.println("Chemin absolue vers l'endroit ou enregistre le fichier");
		sc.nextLine();
		String path =sc.nextLine();
		try {
			this.grapheCourant.fichierEcriture(path);
		} catch (IOException e) {
			System.out.println("Echec de l'exportation du graphe");
		}
	}

	private void ouvrirGraphe() {
		System.out.println("Chemin absolue vers le fichier à ouvrir");
		sc.nextLine();
		String path =sc.nextLine();
		this.grapheCourant=new Graphe(path);
	}

	public void creerGraphe() {
		System.out.println("Nom du graphe :");
		sc.nextLine();
		String nom=sc.nextLine();
		int entree;
		System.out.println("Type de graphes :");
		System.out.println("1 - Plan");
		System.out.println("2 - Ordonnancement de tâches");
		entree = sc.nextInt();
		if(entree==1) {
			Boolean oriente=false;
			Boolean value=false;
			System.out.println("Le graphe est-il orienté [Y/N]");
			sc.nextLine();
			char isOriente=sc.next().charAt(0);
			if(isOriente=='Y'||isOriente=='y') {
				oriente=true;
			}
			
			System.out.println("Le graphe est-il valué ? [Y/N]");
			char isValue=sc.next().charAt(0);
			if(isValue=='Y'||isValue=='y') {
				value=true;
			}
			
			this.grapheCourant= new Graphe(nom, oriente, value);
			creerGraphePlan();
		}else if(entree==2) {
			this.grapheCourant= new Graphe(nom, true, true);
			creerGrapheOrdonnancement();
		}
	}
	
	public void creerGraphePlan() {
		System.out.println("Entrez succesivement le nom des sommets");
		sc.nextLine();
		String nomSommet=sc.nextLine();
		while(!nomSommet.isEmpty()) {
			this.grapheCourant.addSommet(nomSommet);
			nomSommet=sc.nextLine();
		}
		this.grapheCourant.printSommets();
		
		System.out.println("Ajouter des arcs ? [Y/N]");
		char another=sc.next().charAt(0);
		if(another=='Y'||another=='y') {
			ajouterArc();
		}else {
			System.out.println(this.grapheCourant.getNom()+" est désormais le graphe courant");
			menu();
		}
	}
	
	public void creerGrapheOrdonnancement() {
		System.out.println("Nom de la tâche");
		sc.nextLine();
		String nomTache=sc.nextLine();
		this.grapheCourant.addSommet(nomTache);
		System.out.println("Durée de la tâche");
		double valeur = sc.nextDouble();
		System.out.println("Entrez successivement le numéro des tâches nécessaires à la réalisation de cette dernière");
		this.grapheCourant.printSommets();
		sc.nextLine();
		String pred=sc.nextLine();
		while(!pred.isEmpty()) {
			this.grapheCourant.getStruct().addSuccesseur(Integer.parseInt(pred), this.grapheCourant.getNbSommets()-1, valeur);
			pred=sc.nextLine();
		}
		System.out.println("Ajouter une autre tâche ? [Y/N]");
		char another=sc.next().charAt(0);
		if(another=='Y'||another=='y') {
			creerGrapheOrdonnancement();
		}else {
			System.out.println(this.grapheCourant.getNom()+" est désormais le graphe courant");
			menu();
		}
		
	}
	
	public void ajouterArc() {
		if(this.grapheCourant.isOriente()) {
			System.out.println("Numéro du sommet source");
			int pred = sc.nextInt();
			System.out.println("Numéro du sommet destination");
			int suc = sc.nextInt();
			if(this.grapheCourant.isValue()) {
				System.out.println("Valeur de l'arc");
				double valeur = sc.nextDouble();
				this.grapheCourant.getStruct().addSuccesseur(pred, suc, valeur);
			}else {
				this.grapheCourant.getStruct().addSuccesseur(pred, suc);			
			}
		}else{
			System.out.println("Numéro du premier sommet");
			int pred = sc.nextInt();
			System.out.println("Numéro du second sommet");
			int suc = sc.nextInt();
			if(this.grapheCourant.isValue()) {
				System.out.println("Valeur de l'arc");
				double valeur = sc.nextDouble();
				this.grapheCourant.getStruct().addSuccesseur(pred, suc, valeur);
				this.grapheCourant.getStruct().addSuccesseur(suc, pred, valeur);
			}else {
				this.grapheCourant.getStruct().addSuccesseur(pred, suc);
				this.grapheCourant.getStruct().addSuccesseur(suc, pred);
			}
		}
		
		System.out.println("Ajouter un autre arc ? [Y/N]");
		sc.nextLine();
		char another=sc.next().charAt(0);
		if(another=='Y'||another=='y') {
			ajouterArc();
		}else {
			System.out.println(this.grapheCourant.getNom()+" est désormais le graphe courant");
			menu();
		}
		
		
		ajouterArc();
	}
	
	public void afficherGraphe() {
		System.out.println(this.grapheCourant);
	}
}
