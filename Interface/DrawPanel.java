package Interface;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Polygon;
import java.awt.RenderingHints;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.geom.Line2D;
import java.util.ArrayList;

import javax.sound.sampled.Line;
import javax.swing.JFrame;
import javax.swing.JPanel;

import Graphe.Graphe;
import Graphe.Sommet;

public class DrawPanel extends JPanel {
    private static final long serialVersionUID = 1L;
    private ArrayList<Point> points;
    private Graphe graphe;
	private ArrayList<Integer> resDijkstra;

    public DrawPanel() {
        points = new ArrayList<Point>();
    }

    public DrawPanel(Graphe graphe) {
    	this.graphe=graphe;
		this.resDijkstra=null;

    	points = new ArrayList<Point>();
    	
    	int w = 25;
    	int h = 0;
    	
    	for(int i=0; i<graphe.getNbSommets();i++) {
    		points.add(new Point(-1,-1));
    	}
    	
    	points.get(0).setLocation(w, h);
    	ArrayList<Integer> succ=graphe.getStruct().getSuccesseurs(0);
    	
    	calculCoordonnes(succ, w+150, h);
   
	}

	public DrawPanel(Graphe graphe, ArrayList<Integer> res) {
		this.resDijkstra=res;
    	this.graphe=graphe;
    	points = new ArrayList<Point>();
    	
    	int w = 25;
    	int h = 0;
    	
    	for(int i=0; i<graphe.getNbSommets();i++) {
    		points.add(new Point(-1,-1));
    	}
    	
    	points.get(0).setLocation(w, h);
    	ArrayList<Integer> succ=graphe.getStruct().getSuccesseurs(0);
    	
    	calculCoordonnes(succ, w+150, h);
	}

	private void calculCoordonnes(ArrayList<Integer> succ, int w, int h) {
		int ht=h;
		ArrayList<Integer> tocalcul=new ArrayList<Integer>();
		
		for(int i=0; i<succ.size();i++) {
			int numSommet=succ.get(i);
			
			if(this.points.get(numSommet).getX()==-1) {
				points.set(numSommet, new Point(w,ht));
				ht+=150;
				tocalcul.add(numSommet);
			}
		}
		
		if(tocalcul.size()==0) {
			for(int i=0;i<this.points.size();i++) {
				if(this.points.get(i).getY()==-1) {
					points.set(i, new Point(w,ht));
					ht+=150;
				}
			}
		}
		
		for(int i=0; i<tocalcul.size();i++) {
			int numSommet=tocalcul.get(i);
			calculCoordonnes(graphe.getStruct().getSuccesseurs(numSommet), w+150, h);
		}
		
	}

	@Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D g2 = (Graphics2D) g;
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

        g2.setColor(Color.white);
        g2.setStroke(new BasicStroke(5));

        for(int i=0;i<this.graphe.getNbSommets();i++) {
       		ArrayList<Integer> succ=graphe.getStruct().getSuccesseurs(i);
       		for(int j=0; j<succ.size();j++) {
           		int x1=(int) this.points.get(i).getX()+25;
           		int y1=(int) this.points.get(i).getY()+25;
           		int x2=(int) this.points.get(succ.get(j)).getX()+25;
           		int y2=(int) this.points.get(succ.get(j)).getY()+25;
           		g2.drawLine(x1, y1, x2, y2);
           		g2.setColor(new Color(255, 247, 105));
           		g2.drawString(String.valueOf(this.graphe.getStruct().getArc(i, succ.get(j))), (x1+x2)/2, (y1+y2)/2-5);
           		g2.setColor(Color.white);
        	}
        		
        }

        if(this.resDijkstra!=null) {
        	g2.setColor(new Color(84, 255, 104));
        	for(int t=0;t<this.resDijkstra.size()-1;t++) {
           		int x1=(int) this.points.get(this.resDijkstra.get(t)).getX()+25;
           		int y1=(int) this.points.get(this.resDijkstra.get(t)).getY()+25;
           		int x2=(int) this.points.get(this.resDijkstra.get(t+1)).getX()+25;
           		int y2=(int) this.points.get(this.resDijkstra.get(t+1)).getY()+25;
           		g2.drawLine(x1, y1, x2, y2);
           		
        	}
        }
        
        if(this.graphe.isOriente()) {
           	g2.setColor(new Color(110, 146, 255));
	        for(int i=0;i<this.graphe.getNbSommets();i++) {
	       		ArrayList<Integer> succ=graphe.getStruct().getSuccesseurs(i);
	       		for(int j=0; j<succ.size();j++) {
	           		int x1=(int) this.points.get(i).getX()+25;
	           		int y1=(int) this.points.get(i).getY()+25;
	           		int x2=(int) this.points.get(succ.get(j)).getX()+25;
	           		int y2=(int) this.points.get(succ.get(j)).getY()+25;
		           	int cx=(x1+x2)/2;
		           	int cy=(y1+y2)/2;
		           		
		           	Polygon polygon = new Polygon();
		           	
		           	if(x1>x2&&y1>y2) {
		           			polygon.addPoint(cx, cy);
		           			polygon.addPoint(cx, cy+10);
		           			polygon.addPoint(cx+10, cy);
		           	}else if(x1<x2&&y1<y2) {
		           			polygon.addPoint(cx, cy);
		           			polygon.addPoint(cx, cy-10);
		           			polygon.addPoint(cx-10, cy);
		           	}else if(x1<x2&&y1>y2) {
	           			polygon.addPoint(cx, cy);
	           			polygon.addPoint(cx-10, cy);
	           			polygon.addPoint(cx, cy+10);
		           	}else if(x1>x2&&y1<y2) {
	           			polygon.addPoint(cx, cy);
	           			polygon.addPoint(cx, cy-10);
	           			polygon.addPoint(cx+10, cy);
		           	}else if(x1<x2&&y1==y2) {
	           			polygon.addPoint(cx, cy);
	           			polygon.addPoint(cx-10, cy-5);
	           			polygon.addPoint(cx-10, cy+5);
		           	}else if(x1>x2&&y1==y2) {
	           			polygon.addPoint(cx, cy);
	           			polygon.addPoint(cx+10, cy-5);
	           			polygon.addPoint(cx+10, cy+5);
		           	}else if(x1==x2&&y1<y2) {
	           			polygon.addPoint(cx, cy);
	           			polygon.addPoint(cx-5, cy-10);
	           			polygon.addPoint(cx+5, cy-10);
		           	}else if(x1==x2&&y1<y2) {
	           			polygon.addPoint(cx, cy);
	           			polygon.addPoint(cx-5, cy+10);
	           			polygon.addPoint(cx+5, cy+10);
		           	}
		           	
		           	 g2.fillPolygon(polygon);
		           	 g2.drawPolygon(polygon);
	        	}
	        		
	        }
        }
        

        g.setFont(g.getFont().deriveFont(g.getFont().getSize() * 1.6F));
        
        
        int i=0;
        for (Point point : points) {
        	g2.setColor(new Color(255, 92, 114));
            g2.fillOval(point.x, point.y, 50, 50);
            g2.setColor(Color.white);
            g2.drawString(String.valueOf(i), point.x+20, point.y+30);
            i++;
        }
        
        int xs=1250;
        int ys=500;
        
        for (Sommet sommet : this.graphe.getSommets()) {
            g2.drawString(sommet.toString(), xs, ys);
           	ys+=25;
        }
        

    }
}