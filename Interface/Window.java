package Interface;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Frame;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Insets;
import java.awt.Polygon;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;

import Graphe.Graphe;
import Graphe.Sommet;

public class Window extends JFrame{
	
	private JPanel sidebar;
	private JPanel titre;
	private JPanel graphe;
	private JPanel resultat;
	
	private Graphe grapheCourant;
	static final String DEFAULT_GRAPH_PATH = "src/Data/default.csv";


	public Window(){
		super("Projet graphes");
		this.grapheCourant=new Graphe(DEFAULT_GRAPH_PATH);
		initContent();
		
	}
	
	public void initContent() {
		
		
		
		//Sidebar
		this.sidebar=new JPanel();
		sidebar.setLayout(new BoxLayout(sidebar, BoxLayout.PAGE_AXIS));
		JButton button_a=new JButton("Ouvrir un graphe");
		JButton button_b=new JButton("Exporter un graphe");
		JButton button_c=new JButton("Créer un graphe");
		JButton button_d=new JButton("Chemin le plus cours");
		JButton button_e=new JButton("Ajouter un arc");
		JButton button_f=new JButton("Supprimer un arc");
		JButton button_g=new JButton("Ajouter un sommet");
		JButton button_h=new JButton("Supprimer un sommet");
		
		button_a.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ouvrirGraphe();
			}
		});
		
		button_b.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				exporterGraphe();
			}
		});
		
		button_c.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				nouveauGraphe();
			}
		});
		
		button_d.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dijkstra();
			}
		});
		
		button_e.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ajoutArc();
			}
		});
		
		button_f.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				supprimerArc();
			}
		});
		
		button_g.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ajouterSommet();
			}
		});
		
		button_h.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				supprimerSommet();
			}
		});
		
		sidebar.add(button_a);
		sidebar.add(Box.createRigidArea(new Dimension(0, 20)));
		sidebar.add(button_b);
		sidebar.add(Box.createRigidArea(new Dimension(0, 20)));
		sidebar.add(button_c);
		sidebar.add(Box.createRigidArea(new Dimension(0, 20)));
		sidebar.add(button_d);
		sidebar.add(Box.createRigidArea(new Dimension(0, 20)));
		sidebar.add(button_e);
		sidebar.add(Box.createRigidArea(new Dimension(0, 20)));
		sidebar.add(button_f);
		sidebar.add(Box.createRigidArea(new Dimension(0, 20)));
		sidebar.add(button_g);
		sidebar.add(Box.createRigidArea(new Dimension(0, 20)));
		sidebar.add(button_h);

		
		
		sidebar.setBackground(Color.BLACK);
		sidebar.setBorder(new EmptyBorder(20, 20, 20, 20));
		button_a.setBackground(new Color(160, 255, 148));
		button_b.setBackground(new Color(160, 255, 148));
		button_c.setBackground(new Color(160, 255, 148));
		button_d.setBackground(new Color(140, 245, 243));
		button_e.setBackground(new Color(140, 245, 243));
		button_f.setBackground(new Color(140, 245, 243));
		button_g.setBackground(new Color(140, 245, 243));
		button_h.setBackground(new Color(140, 245, 243));
		

		
		button_a.setMaximumSize(new Dimension(sidebar.getMaximumSize()));
		button_b.setMaximumSize(new Dimension(sidebar.getMaximumSize()));
		button_c.setMaximumSize(new Dimension(sidebar.getMaximumSize()));
		button_d.setMaximumSize(new Dimension(sidebar.getMaximumSize()));
		button_e.setMaximumSize(new Dimension(sidebar.getMaximumSize()));
		button_f.setMaximumSize(new Dimension(sidebar.getMaximumSize()));
		button_g.setMaximumSize(new Dimension(sidebar.getMaximumSize()));
		button_h.setMaximumSize(new Dimension(sidebar.getMaximumSize()));
		
		button_a.setFont(button_a.getFont().deriveFont (16.0f));
		button_b.setFont(button_a.getFont().deriveFont (16.0f));
		button_c.setFont(button_a.getFont().deriveFont (16.0f));
		button_d.setFont(button_a.getFont().deriveFont (16.0f));
		button_e.setFont(button_a.getFont().deriveFont (16.0f));
		button_f.setFont(button_a.getFont().deriveFont (16.0f));
		button_g.setFont(button_a.getFont().deriveFont (16.0f));
		button_h.setFont(button_a.getFont().deriveFont (16.0f));
		
		//Titre
		this.titre=new JPanel();
		JLabel t= new JLabel();
		t.setText(this.grapheCourant.getNom());
		t.setForeground(Color.WHITE);
		t.setFont (t.getFont ().deriveFont (64.0f));
		titre.add(t);
		titre.setBackground(Color.DARK_GRAY);
		
		//Graphe
		this.graphe=new DrawPanel(this.grapheCourant);
		graphe.setBackground(Color.DARK_GRAY);

		//Résultat
		this.resultat=new JPanel();
		JButton f=new JButton("Res");
		resultat.add(f);
		
		//Droite
		JPanel droite=new JPanel();
		droite.setLayout(new BorderLayout());
		droite.add(titre,"North");
		droite.add(graphe,"Center");
		//droite.add(resultat,"South");
		
		//Global
		getContentPane().add(sidebar, "West") ;
		getContentPane().add(droite, "Center") ;
		pack() ;
		setDefaultCloseOperation(EXIT_ON_CLOSE) ;
		setVisible(true) ;

	}

	protected void nouveauGraphe() {
        String name = JOptionPane.showInputDialog("Nom du graphe");

        int o = JOptionPane.showConfirmDialog(
        	    this,
        	    "Le graphe est-il orienté ?",
        	    "",
        	    JOptionPane.YES_NO_OPTION);
        
        int v = JOptionPane.showConfirmDialog(
        	    this,
        	    "Le graphe est-il valué ?",
        	    "",
        	    JOptionPane.YES_NO_OPTION);
        boolean value=true;
        boolean oriente=true;
        if(o==1) {
        	oriente=false;
        }
        if(v==1) {
        	value=false;
        }
		
        this.grapheCourant=new Graphe(name, oriente,value);
		getContentPane().setVisible(false);
		getContentPane().removeAll();
		
		JPanel droite=new JPanel();
		droite.setLayout(new BorderLayout());
		
		this.titre=new JPanel();
		JLabel t= new JLabel();
		t.setText(this.grapheCourant.getNom());
		t.setForeground(Color.WHITE);
		t.setFont (t.getFont ().deriveFont (64.0f));
		titre.add(t);
		titre.setBackground(Color.DARK_GRAY);
		
		this.graphe=new JPanel();
		graphe.setBackground(Color.DARK_GRAY);
		droite.add(titre,"North");
		droite.add(graphe,"Center");
		getContentPane().add(sidebar, "West") ;
		getContentPane().add(droite, "Center");
		getContentPane().setVisible(true);
	}

	protected void supprimerSommet() {
		Sommet[] sommets = new Sommet[this.grapheCourant.getNbSommets()];
		int i=0;
		for(Sommet sommet : this.grapheCourant.getSommets()) {
			sommets[i]=sommet;
			i++;
		}
		Sommet s1 = (Sommet) JOptionPane.showInputDialog(this,
		                    "Premier Sommet :",
		                    "",
		                    JOptionPane.PLAIN_MESSAGE,
		                    null,
		                    sommets, this.grapheCourant.getSommets().get(0));
		
		this.grapheCourant.deleteSommet(s1.getKey());
		this.majAffichage();
	}

	protected void ajouterSommet() {
	    String nom = JOptionPane.showInputDialog("Nom du sommet :");
	    this.grapheCourant.addSommet(nom);
		this.majAffichage();
	}

	protected void supprimerArc() {
		Sommet[] sommets = new Sommet[this.grapheCourant.getNbSommets()];
		int i=0;
		for(Sommet sommet : this.grapheCourant.getSommets()) {
			sommets[i]=sommet;
			i++;
		}
		Sommet s1 = (Sommet) JOptionPane.showInputDialog(this,
		                    "Premier Sommet :",
		                    "",
		                    JOptionPane.PLAIN_MESSAGE,
		                    null,
		                    sommets, this.grapheCourant.getSommets().get(0));
		
		Sommet s2 = (Sommet) JOptionPane.showInputDialog(this,
                "Deuxieme Sommet:",
                "",
                JOptionPane.PLAIN_MESSAGE,
                null,
                sommets, this.grapheCourant.getSommets().get(0));
		
		this.grapheCourant.getStruct().deleteSuccesseur(s1.getKey(), s2.getKey());
		if(!this.grapheCourant.isOriente()){
			this.grapheCourant.getStruct().deleteSuccesseur(s2.getKey(), s1.getKey());
		}
		
		this.majAffichage();
		
	}

	protected void ajoutArc() {
		Sommet[] sommets = new Sommet[this.grapheCourant.getNbSommets()];
		int i=0;
		for(Sommet sommet : this.grapheCourant.getSommets()) {
			sommets[i]=sommet;
			i++;
		}
		Sommet s1 = (Sommet) JOptionPane.showInputDialog(this,
		                    "Premier Sommet :",
		                    "",
		                    JOptionPane.PLAIN_MESSAGE,
		                    null,
		                    sommets, this.grapheCourant.getSommets().get(0));
		
		Sommet s2 = (Sommet) JOptionPane.showInputDialog(this,
                "Deuxieme Sommet:",
                "",
                JOptionPane.PLAIN_MESSAGE,
                null,
                sommets, this.grapheCourant.getSommets().get(0));
		
		double value=1;
		
		if(this.grapheCourant.isValue()) {
	        String v = JOptionPane.showInputDialog("Valeur de l'arc");
	        value = Double.valueOf(v);
		}
		
		this.grapheCourant.getStruct().addSuccesseur(s1.getKey(), s2.getKey(),value);
		if(!this.grapheCourant.isOriente()) {
			this.grapheCourant.getStruct().addSuccesseur(s2.getKey(), s1.getKey(),value);
		}
		
		this.majAffichage();
	}

	protected void exporterGraphe() {
	    JFileChooser chooser = new JFileChooser(); 
	    chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
	    chooser.setAcceptAllFileFilterUsed(false);  
	    chooser.showOpenDialog(this);
		try {
			this.grapheCourant.fichierEcriture(chooser.getSelectedFile().getAbsolutePath());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	protected void ouvrirGraphe() {
        JFileChooser chooser = new JFileChooser();
        chooser.showOpenDialog(getParent());
        File fichier = chooser.getSelectedFile();
        String path = fichier.getPath();
        this.grapheCourant=new Graphe(path);
        this.graphe=new DrawPanel(this.grapheCourant);
        majAffichage();
	}

	private void majAffichage() {
		getContentPane().setVisible(false);
		getContentPane().removeAll();
		
		JPanel droite=new JPanel();
		droite.setLayout(new BorderLayout());
		
		this.titre=new JPanel();
		JLabel t= new JLabel();
		t.setText(this.grapheCourant.getNom());
		t.setForeground(Color.WHITE);
		t.setFont (t.getFont ().deriveFont (64.0f));
		titre.add(t);
		titre.setBackground(Color.DARK_GRAY);
		
		this.graphe=new DrawPanel(this.grapheCourant);
		graphe.setBackground(Color.DARK_GRAY);
		droite.add(titre,"North");
		droite.add(graphe,"Center");
		getContentPane().add(sidebar, "West") ;
		getContentPane().add(droite, "Center");
		getContentPane().setVisible(true);

	}
	
	private void dijkstra() {
		Sommet[] sommets = new Sommet[this.grapheCourant.getNbSommets()];
		int i=0;
		for(Sommet sommet : this.grapheCourant.getSommets()) {
			sommets[i]=sommet;
			i++;
		}
		Sommet s1 = (Sommet) JOptionPane.showInputDialog(this,
		                    "Sommet de départ :",
		                    "",
		                    JOptionPane.PLAIN_MESSAGE,
		                    null,
		                    sommets, this.grapheCourant.getSommets().get(0));
		
		Sommet s2 = (Sommet) JOptionPane.showInputDialog(this,
                "Sommet de destination :",
                "",
                JOptionPane.PLAIN_MESSAGE,
                null,
                sommets, this.grapheCourant.getSommets().get(0));
		
		System.out.println(s1);
		System.out.println(s2);
		
		ArrayList<Integer> res = this.grapheCourant.Dijkstra(s1.getKey(), s2.getKey());
		this.graphe= new DrawPanel(this.grapheCourant,res);
	
		this.majAffichage();
	}
	
		

}