package Graphe;

import java.util.ArrayList;

public abstract class Structure {
	
	/**
	 * @param numSommet : clé du sommet dont on veut récupérer les successeurs
	 * @return une arrayliste contenant les clés des successeurs
	 */
	public abstract ArrayList<Integer> getSuccesseurs(int numSommet);
	
	/**
	 * @param numSommet : clé du sommet dont on veut récupérer les prédecesseurs
	 * @return
	 */
	public abstract ArrayList<Integer> getPredecesseurs(int numSommet);
	
	/**
	 * @param pred : clé du somme source
	 * @param suc : clé du sommet destination
	 * @param valeur : valeur pour aller du somme pred vers suc
	 */
	public abstract void addSuccesseur(int pred,int suc,double valeur);
	
	/**
	 * @param pred : clé du somme source
	 * @param suc : clé du sommet destination
	 * A utiliser dans le cas des graphes non valués ou tout les arc on 1 comme valeur
	 */
	public abstract void addSuccesseur(int pred,int suc);
	
	/**
	 * @param pred : clé du somme source
	 * @param suc : clé du sommet destination
	 * Supprime l'arc de pred à suc
	 */
	public abstract void deleteSuccesseur(int pred, int suc);
	
	/**
	 * @param numSommet : clé du sommet à ajouter
	 */
	public abstract void addSommet();
	
	
	/**
	 * @param numSommet : clé du sommet à supprimer
	 */
	public abstract void deleteSommet(int numSommet);

    public abstract String toPrint();

	public abstract double getArc(int j, int numSommet);
}
