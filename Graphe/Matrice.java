package Graphe;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.StringJoiner;

public class Matrice extends Structure{
	
	private double[][] matrice;
	
	
	public Matrice() {
		this(0);
	}
    public Matrice(int numsommet) {
		this.matrice= new double[numsommet][numsommet];
		for (double x[]: this.matrice) {
			Arrays.fill(x, Double.POSITIVE_INFINITY);
		}
	}
    /*public Matrice(FsAps fsaps) {
    	this.matrice= new double [fsaps.getAps().size()][fsaps.getAps().size()];
    		for (int i=0;i<fsaps.getAps().size();i++) {
    			for (int j=0; j<fsaps.getSuccesseurs(i).size();j++) {
    				if(fsaps.getSuccesseurs(i).isEmpty() {
    					this.matrice[i][j]= Double.POSITIVE_INFINITY;
    				}
    				else {
    					this.matrice[i][j]= fsaps.getVs().get(j);
    				}
    				
    			}
    	  }
    }
    */
    public double[][] getMatrice() {
		return matrice;
	}
	@Override
	public ArrayList<Integer> getSuccesseurs(int numSommet) {
		ArrayList<Integer> A = new ArrayList<Integer>();
			for(int j=0;j<this.matrice.length;j++)
			{
				if (this.matrice[numSommet][j]!= Double.POSITIVE_INFINITY) {
					A.add(j);
				}
			}
		return A;
	}

	@Override
	public ArrayList<Integer> getPredecesseurs(int numSommet) {
		ArrayList<Integer> A = new ArrayList<Integer>();
		for(int i=0;i<this.matrice.length;i++)
		{
			if (this.matrice[i][numSommet]!= Double.POSITIVE_INFINITY) {
				A.add(i);
			}
		}
	     return A;
	}

	@Override
	public void addSuccesseur(int pred, int suc, double valeur) {
		this.matrice[suc][pred]=valeur;
		
	}

	@Override
	public void addSuccesseur(int pred, int suc) {
		addSuccesseur(pred,suc,1);
		
	}

	@Override
	public void deleteSuccesseur(int pred, int suc) {
		this.matrice[suc][pred]=Double.POSITIVE_INFINITY;
		//this.matrice[pred][suc]=Double.POSITIVE_INFINITY;
		
	}

	@Override
	public void addSommet() {
		double[][] m = new double[this.matrice.length+1][this.matrice.length+1];
		for(int i=0;i<=m.length-1;i++)
			if(i==m.length-1) {
			Arrays.fill(m[i],Double.POSITIVE_INFINITY);
			}
			else {
			for(int j=0;j<=m[0].length-1;j++)
			{
				if(j==m.length-1) {
					m[i][j]=Double.POSITIVE_INFINITY;
					}
				else {
				m[i][j]=this.matrice[i][j];
				}
			}
			}
		
		 
		 this.matrice=m; 
	}

	@Override
	public void deleteSommet(int numSommet) {
		double[][] m = new double[this.matrice.length-1][this.matrice.length-1];
	    int p=0;
	    for( int i = 0; i <this.matrice.length; ++i)
	    {
	        if ( i == numSommet)
	            continue;
	        int q = 0;
	        for( int j = 0; j <this.matrice[0].length; ++j)
	        {
	            if ( j == numSommet)
	                continue;

	            m[p][q] = this.matrice[i][j];
	            ++q;
	        }
	        ++p;
	    }
	        
	 this.matrice=m;
}
	public String toString() {
		StringBuilder s1= new StringBuilder();
		
		s1.append(String.format("%4s", "|"));
		for(int e=0;e<=this.matrice.length-1;e++){
			s1.append(String.format("%4s", e+"|"));
		}
		s1.append("\n");
		
        for(int i=0;i<=this.matrice.length-1;i++){
        	s1.append(String.format("%4s", i+"|"));
        	for(int j=0;j<=this.matrice.length-1;j++)
            {
        		
            	if(this.matrice[i][j]==Double.POSITIVE_INFINITY) {
            		s1.append(String.format("%4s", "__|"));
            	}else {
            		s1.append(String.format("%4s", this.matrice[i][j]+"|"));
            	}
            	
            }
            s1.append("\n");
        }
      return s1.toString();
	}
	
	public String toPrint() {
	StringBuilder s1= new StringBuilder();
        for (double[] x : this.matrice)
        {
            for (double y : x)
            {   
            	if (y==Double.POSITIVE_INFINITY) {
            		s1.append("x,");
            	}
            	else {
            	s1.append(String.format("%8s,", y));
            	}
            }
            s1.append("\n");
        }
      return s1.toString();
		}
	   public double getArc(int s1, int s2) {
		   return this.matrice[s1][s2];
	   }

	}

   
