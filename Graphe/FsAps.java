package Graphe;

import java.util.ArrayList;

public class FsAps extends Structure{
	
	private ArrayList<Integer> aps;
	private ArrayList<Integer> fs;
	private ArrayList<Double> vs;
	
	public FsAps(int nombreSommets) {
		aps=new ArrayList<Integer> ();
		fs=new ArrayList<Integer>();
		vs=new ArrayList<Double>();
		for(int i=0; i<nombreSommets;i++) {
			aps.add(0);
		}
	}
	
	public FsAps() {
		this(0);
	}
	
	public ArrayList<Integer> getAps(){
		return aps;
	}
	
	public ArrayList<Integer> getFs(){
		return fs;
	}
	
	public ArrayList<Double> getVs(){
		return vs;
	}
	
	@Override
	public ArrayList<Integer> getSuccesseurs(int numSommet) {
		ArrayList<Integer> successeurs=new ArrayList<Integer> ();
		int indiceDernierElement;
		if(numSommet<aps.size()-1) {
			indiceDernierElement=aps.get(numSommet+1);
		}
		else {
			if(numSommet==aps.size()-1) {
				indiceDernierElement=fs.size();
			}
			else
				return successeurs;
		}
		for(int i=aps.get(numSommet);i<indiceDernierElement;i++) {
			successeurs.add(fs.get(i));
		}
		return successeurs;
	}
	
	@Override
	public ArrayList<Integer> getPredecesseurs(int numSommet) {
		ArrayList<Integer> predecesseurs=new ArrayList<Integer>();
		for(int i=0;i<aps.size()-1;i++) {
			int j=aps.get(i);
			int k=aps.get(i+1);
			if(j!=k) {
				//ici, il se peut souvent que la le sommet n'apparesse pas comme successeur, 
				//c'est pourquoi il est int�ressant de faire le test fs.get(j)<numSommet
				while(j<k && fs.get(j)<numSommet) {
					j++;
				}
				if(j<k && fs.get(j)==numSommet) {
					predecesseurs.add(i);
				}
			}
		}
		//cas du dernier noeud
		int l=aps.get(aps.size()-1);
		if(l!=fs.size()) {
			while(l<fs.size()&&fs.get(l)<numSommet) {
				l++;
			}
			if(l<fs.size() && fs.get(l)==numSommet) {
				predecesseurs.add(aps.size()-1);
			}
		}
		return predecesseurs;
	}
	
	@Override
	public void addSommet() {
		// TODO Auto-generated method stub
		if(aps.size()==0) {
			aps.add(0);
		}
		else {
			aps.add(fs.size());  //j'avais mis ce caca je ne sais pas pourquoi : aps.add(aps.get(aps.size()-1));
		}
	}
	
	@Override 
	/**
	 * ajout d'un arc entre deux sommets qui existent d�j�
	 * suc est le sommet successeur de pred
	 * valeur est la valeur de l'arc entre le sommet pred et le sommet suc
	 * */
	public void addSuccesseur(int pred, int suc, double valeur) {
		//si le sommet suc ou pred n'existe pas, il faut les cr�er
		while(suc>=aps.size() || pred>=aps.size()) {
			this.addSommet();
		}
		
		//cherche sa place dans le tableau fs (et donc vs par la m�me occasion)
		//ici on pourrait tr�s bien enregistrer plusieurs fois le m�me successeur avec une valeur diff�rente
		int i=aps.get(pred);		
		int borne;
		if(pred < aps.size()-1) {	borne = aps.get(pred+1);}
		else { borne = fs.size(); }
		
		while(i<borne && fs.get(i)<suc)
		{	i++;}
		if(i<borne && fs.get(i)==suc) {//si un arc avait d�j� �t� enregistr�
			vs.set(i, valeur);
		}
		else {
			fs.add(i,suc);
			vs.add(i,valeur);
			
			//aps : incr�mmenter les indices des sommets suivants
			for(int j=pred+1;j<aps.size();j++) {
				aps.set(j, aps.get(j)+1);
			}
		}
	}
	
	/*
	 * 1 est la valeur par d�faut d'un arc
	 */
	@Override
	public void addSuccesseur(int pred, int suc) {
		// TODO Auto-generated method stub
		addSuccesseur(pred,suc,1);		
	}
	
	
	@Override
	public void deleteSuccesseur(int pred, int suc) {
		// TODO Auto-generated method stub
		//chercher la valeur dans fs et la suppprimer
		int i=aps.get(pred);
		int indiceBorne;
		if(pred<aps.size()-1) {// cas 1 : ce n'est pas le dernier noeud
			indiceBorne=aps.get(pred+1);
		}
		else { //cas : pred est le dernier noeud
			indiceBorne=fs.size();
		}
		while(i<indiceBorne && fs.get(i)<suc) {
			i++;
		}
		if(i<indiceBorne && fs.get(i)==suc) {
			fs.remove(i);
			vs.remove(i);
			for(int j=pred+1;j<aps.size();j++) {
				aps.set(j, aps.get(j)-1);
			}
		}
	}
	
	
	@Override
	public void deleteSommet(int numSommet) {
		// TODO Auto-generated method stub
		for(int i=0; i<numSommet; i++) {
			deleteSuccesseur(i,numSommet);
		}
		
		if(numSommet==aps.size()-1) { // si le sommet � supprimer est le dernier
			for(int i=fs.size()-1; i>=aps.get(numSommet); i--) { //v�rifier l'initialisation de i
				fs.remove(i);
				vs.remove(i);
			}
			aps.remove(numSommet);
		}
		else {
			int cpt=0;
			for(int i=aps.get(numSommet+1)-1; i>=aps.get(numSommet); i--) {
				fs.remove(i);
				vs.remove(i);
				cpt++;
			}
			aps.remove(numSommet);
			//boucle temporaire
			for(int k=numSommet; k<aps.size();k++) {
				aps.set(k, aps.get(k)-cpt);
			}
		}
		
		for(int i=numSommet; i<aps.size()-1; i++) {
			deleteSuccesseur(i,numSommet);
		}

		//d�cr�menter les successeurs sup�rieurs au sommet supprim�
		/**int i=aps.size()-1;
		int j=fs.size()-1;
		while(i>=numSommet) {
			while(j>aps.get(i) && fs.get(j)>numSommet) {
			}
			i--;
			j=;
		}*/
		for(int i=0;i<fs.size();i++) {
			if(fs.get(i)>numSommet) {
				fs.set(i,fs.get(i)-1);
			}
		}
	}
	
	@Override
	public String toPrint() {
		if(aps.size()==0) {
			return "Ce graphe ne contient pas de noeuds";
		}
		else {
			StringBuilder s= new StringBuilder();
			for(int i = 0 ; i < aps.size()-1; i++) {
				s.append("noeud " + i + " : \t");
				if(aps.get(i)!=aps.get(i+1)) {
					for(int j=aps.get(i);j<aps.get(i+1);j++) {
						s.append("\t" + fs.get(j) + " (" + vs.get(j) + ") ");
					}
				}
				s.append("\n");
			}
			
			//dernier noeud
			int i=aps.size()-1;
			s.append("noeud " + i + " : \t");
			if(aps.get(i)<aps.size()) {
				for(int j=aps.get(i);j<fs.size();j++) {
					s.append("\t" + fs.get(j) + " (" + vs.get(j) + ") ");
				}
				s.append("\n");
			}
			s.append("\n");
			return s.toString();
		}
	}

	@Override
	public double getArc(int j, int numSommet) {
		// TODO Auto-generated method stub
		return 0;
	}
}
