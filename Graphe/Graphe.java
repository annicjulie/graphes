package Graphe;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.DirectoryStream;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Vector;

public class Graphe {
	private String nom;
	private ArrayList<Sommet> sommets;
	private int nbSommets;
	private Structure struct;
	private boolean oriente;
	private boolean value;

	public Graphe(String nom, Boolean oriente, Boolean value) {
		this.nom=nom;
		this.sommets=new ArrayList<Sommet>();
		this.nbSommets=0;
		this.struct=new Matrice();
		this.oriente=oriente;
		this.value=value;
	}
	
	public Graphe(String filePath){
		this.sommets=new ArrayList<Sommet>();
		this.struct=new Matrice();
		File f = new File(filePath);
		FileInputStream finput;

		try {
			finput = new FileInputStream(f);
			
			readFile(finput);

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}


	}
	
	private void readFile(FileInputStream finput) {
		ArrayList<ArrayList<String>> fileContent = new ArrayList<ArrayList<String>>();
		ArrayList<String> ligne = new ArrayList<String>();

		int c=-1;
		String courant = "";

		do {
			try {
				c = finput.read();
			} catch (IOException e) {
				e.printStackTrace();
			}
			
			if(c==(int)',') {
				ligne.add(courant);
				courant="";
			}else if(c==(int)'\n') {
				ligne.add(courant);
				courant="";
				fileContent.add(ligne);
				ligne=new ArrayList<String>();
			}else {
				courant += (char) c;
				//System.out.println(courant);
			}
		}while(c!=-1);
		
		
		//System.out.println(fileContent.toString());
		this.nom=fileContent.get(0).get(0);
		
		ArrayList<String> som=fileContent.get(1);
		for(int i=0;i<som.size();i++) {
			this.addSommet(som.get(i));
		}
		
		String o=fileContent.get(2).get(0);
		String v=fileContent.get(2).get(1);
		this.oriente=false;
		this.value=false;
		if(o.charAt(0)=='t') {
			this.oriente=true;
		}
		if(v.charAt(0)=='t') {
			this.value=true;
		}
		
		for(int i=0;i<this.nbSommets;i++) {
			for(int j=0;j<this.nbSommets;j++) {
				String value=fileContent.get(i+3).get(j);
				if(value.charAt(0)!='x') {
					this.struct.addSuccesseur(i, j, Double.valueOf(value));
				}
			}
			
		}
	}

	public void addSommet(String nom){
		
		this.sommets.add(new Sommet(this.nbSommets,nom));
		this.nbSommets++;
		this.struct.addSommet();
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public ArrayList<Sommet> getSommets() {
		return sommets;
	}

	public void setSommets(ArrayList<Sommet> sommets) {
		this.sommets = sommets;
	}

	public int getNbSommets() {
		return nbSommets;
	}

	public void setNbSommets(int nbSommets) {
		this.nbSommets = nbSommets;
	}

	public Structure getStruct() {
		return struct;
	}

	public void setStruct(Structure struct) {
		this.struct = struct;
	}
	
	public boolean isOriente() {
		return oriente;
	}

	public void setOriente(boolean oriente) {
		this.oriente = oriente;
	}

	public boolean isValue() {
		return value;
	}

	public void setValue(boolean value) {
		this.value = value;
	}
	
	public void printSommets() {
		  for(int i = 0 ; i < this.sommets.size(); i++) {
			   System.out.println(this.sommets.get(i));
		  }
	}

	@Override
	public String toString() {
		StringBuilder s= new StringBuilder();
		s.append(this.nom+"\n");
		for(int i = 0 ; i < this.sommets.size(); i++) {
			s.append(this.sommets.get(i)+"\n");
		}
		s.append(this.struct);
		
		return s.toString();
	}
	
	
	

	public void fichierEcriture(String chemin) throws IOException {
		String name = chemin +this.nom+".csv";
		File fichier = new File (name);
		FileWriter fw= new FileWriter(fichier);
		PrintWriter pw = new PrintWriter(fw);
		StringBuilder line1 = new StringBuilder();
		for(int i = 0 ; i < this.sommets.size(); i++) {
			line1.append(this.sommets.get(i).getNom()+",");
		}
		String line2 = isOriente() + ","  + isValue();
		pw.println(this.getNom());
		pw.println(line1.toString());
		pw.println(line2);
		pw.println(this.struct.toPrint());
		pw.close();
	}
	public ArrayList<Integer> Dijkstra(int numSommet) {

		//Initialisation
		ArrayList<Double> dist= new ArrayList<Double>();
		ArrayList<Integer> pred= new ArrayList<Integer>();
		ArrayList<Integer> s= new ArrayList<Integer>();
		int j=numSommet;
		
		for(int i=0;i<this.nbSommets;i++) {
			pred.add(-1);
			if(i==numSommet) {
				dist.add((double) 0);
			}else {
				dist.add(Double.POSITIVE_INFINITY);
				s.add(i);
			}

		}
		
		
		do {
			ArrayList<Integer> sucJ = this.struct.getSuccesseurs(j);
			for(int i=0;i<sucJ.size();i++) {
				double tempDist=dist.get(j)+this.struct.getArc(j,sucJ.get(i));
				if(s.contains(sucJ.get(i))&&tempDist<dist.get(sucJ.get(i))) {
					dist.set(sucJ.get(i), tempDist);
					pred.set(sucJ.get(i),j);
				}
			}
			
			double minDistS=Double.POSITIVE_INFINITY;
			int minDistIndice=-1;
			for(int i=0;i<s.size();i++) {
				if(dist.get(s.get(i))<minDistS) {
					minDistIndice=i;
					minDistS=dist.get(s.get(i));
				}		
			}
			j=s.get(minDistIndice);
			s.remove(minDistIndice);
			
		}while(s.size()>0);
		
		return pred;
	}
	
	public ArrayList<Integer> Dijkstra(int debut, int fin) {
		ArrayList<Integer> predList = Dijkstra(debut);
		ArrayList<Integer> chemin = new ArrayList<Integer>();
		
		int last=fin;
		
		do {
			chemin.add(last);
			last=predList.get(last);
		}while(last!=-1);
		
		ArrayList<Integer> reversed = new ArrayList<Integer>();
		
		for(int i=chemin.size()-1;i>=0;i--) {
			reversed.add(chemin.get(i));
		}
		
		return reversed;
	}

	public void deleteSommet(int num) {
		this.struct.deleteSommet(num);
		this.sommets.remove(num);
		this.nbSommets--;
		for(int i=num;i<this.nbSommets-num;i++) {
			this.sommets.get(i).setKey(this.sommets.get(i).getKey()-1);
		}
		
	}		
	 public int minimum(double distance[],ArrayList <Integer> C) {
	        	int resultat= C.get(0);
	        	for(int i=0;i<C.size();i++) {
					if(distance[resultat]>distance[C.get(i)]) {
						resultat= C.get(i);
					}
				}
		 return resultat;
	        }
	
	public ArrayList<Integer> rang(){
		
		ArrayList<Integer> rang = new ArrayList<Integer>();
		
		for(int i=0;i<this.nbSommets;i++) {
			if(this.struct.getPredecesseurs(i).size()==0){
				rang.add(0);
			}else {
				rang.add((int) Double.POSITIVE_INFINITY);
			}
		}
		
		boolean cycle=false;
		while(cycle==false){
			cycle=true;
			for(int i=0;i<this.nbSommets;i++) {
				if(rang.get(i)==(int) Double.POSITIVE_INFINITY){
					ArrayList<Integer> pred = this.struct.getPredecesseurs(i);
					ArrayList<Integer> predrang = new ArrayList<Integer>();
					for(int j=0; j<pred.size();j++) {
						predrang.add(rang.get(pred.get(j))+1);
					}
					System.out.println(i);
					
					Integer max = Collections.max(predrang);
					if(max!=(int)Double.POSITIVE_INFINITY) {
						cycle=false;
						rang.set(i,max);
					}
				}
			}
			
		}
		
		
		return rang;
	
		
	}
	
	
	
	public ArrayList<Map<String,Double>> ordonnancement() {
		if(this.oriente==true) {
			ArrayList<Integer> rang = rang();
			ArrayList<Integer> ordreCalculSommets =new ArrayList<Integer>();
			for(int i=0;i<=Collections.max(rang);i++) {
				for(int j=0;j<this.nbSommets;j++) {
					if(rang.get(j)==i) {
						ordreCalculSommets.add(j);
					}
				}
				
			}
			
			ArrayList<Map<String,Double>> resultat = new ArrayList<Map<String,Double>>();
			for(int i=0;i<this.nbSommets;i++) {
				resultat.add(new HashMap<String,Double>());
			}
			
			//calcul plus tot
			for(int i=0;i<this.nbSommets;i++) {
				int numSommet=ordreCalculSommets.get(i);
				double max=0;
				ArrayList<Integer> pred=this.struct.getPredecesseurs(numSommet);
				for(int j=0; j<pred.size();j++) {
					double dist=this.struct.getArc(pred.get(j),numSommet)+resultat.get(pred.get(j)).get("tot");
					System.out.println(dist);
	
					if(dist>max) {
						max=dist;
					}
				}
				resultat.get(numSommet).put("tot", max);
			}
			
			
			//plustard
			for(int i=nbSommets-1;i>=0;i--) {
				int numSommet=ordreCalculSommets.get(i);
				double min=resultat.get(ordreCalculSommets.get(nbSommets-1)).get("tot");
				ArrayList<Integer> suc=this.struct.getSuccesseurs(numSommet);
				for(int j=0; j<suc.size();j++) {
					double dist=resultat.get(suc.get(j)).get("tard")-this.struct.getArc(numSommet,suc.get(j));
					System.out.println(dist);
	
					if(dist<min) {
						min=dist;
					}
				}
				resultat.get(numSommet).put("tard", min);
				
			}
			

			return resultat;
			
			
		}
		
		return null;
	}
	
	public Matrice kruskal() {
		Matrice m=new Matrice(this.nbSommets);
		HashMap<String,Double> list = new HashMap<String,Double>();
		for (int i=0; i<this.nbSommets;i++) {
				for (int j=0; j<this.nbSommets;j++) { {
					if (((Matrice)this.struct).getArc(i,j)==Double.POSITIVE_INFINITY){
						continue;
					}
					list.put(i+","+j,((Matrice)this.struct).getArc(i,j));
				}
		}}
		// tri des valeurs
		List<Map.Entry<String, Double>> L =
                new LinkedList<Map.Entry<String, Double>>(list.entrySet());
		Collections.sort(L, new Comparator<Map.Entry<String, Double>>() {
            public int compare(Map.Entry<String, Double> o1,
                               Map.Entry<String, Double> o2) {
                return (o1.getValue()).compareTo(o2.getValue());
            }
        });
        // ajout des valeurs � la matrice du nv graphe
		int [] parent= new int[nbSommets];
		//Arrays.fill(parent, -1);
		for (int r = 0; r < this.nbSommets; r++)
		{ parent[r] = r;}
         int [][] arc = new int [2][L.size()];
         double [] poids = new double [L.size()];
         int j=0;
         for (Map.Entry<String, Double> entry : L) {
        	String [] a = entry.getKey().split(",");
         	arc[0][j] = Integer.valueOf(a[0]);
        	arc[1][j] = Integer.valueOf(a[1]);
        	poids[j] = entry.getValue();
        	j++;
          }
		
		int index = 0, b=0;
            while(index<nbSommets-1){
                //check if adding this edge creates a cycle
                int pred = find(parent, arc[0][b]);
                int succ = find(parent, arc[1][b]);

                if(pred==succ){
                    //ignore, will create cycle
                }else {
                    //add it to our final result
                    m.addSuccesseur(arc[0][b], arc[1][b], poids[b]);;
                    index++;
                    union(parent,pred,succ);
                }
	           b++;
            	}     
		return m;
	}
	private int find(int [] parent,int i){
	    if(parent[i] != i)
	       return find(parent,parent[i]);
	   else return i;
	}
	  
	// Does union of i and j. It returns
	// false if i and j are already in same
	// set.
	private void union(int [] parent,int i, int j)
	{
	    int a = find(parent,i);
	    int b = find(parent,j);
	    parent[b] = a;
	}
	
	private void fortconnexe(int s, int iter, int[] num, int[] ro, boolean[] entarj, Vector pilch, int NBcfc, int[] cfc)
	{

		iter++;
		num[s] = iter; ro[s] = iter;
		entarj[s] = true; pilch.add(s);

		ArrayList<Integer> successeurs = struct.getSuccesseurs(s);

		for(int suc: successeurs)
		{

			if(num[suc] == 0)
			{
				fortconnexe(suc, iter, num, ro, entarj, pilch, NBcfc, cfc);
				if(ro[suc] < ro[s])
				{ ro[s] = ro[suc]; }
			}
			else
			{
				if(num[suc] < ro[s] && entarj[suc])
				{ ro[s] = num[suc]; }
			}
		}

		if(ro[s] == num[s])
		{
			NBcfc++;

			while ((Integer)pilch.lastElement() >= num[s])
			{
				entarj[(Integer)pilch.lastElement()] = false;
				cfc[(Integer)pilch.lastElement()] = NBcfc;
				pilch.remove(pilch.size() - 1);
			}
		}

	}

	public Graphe tarjan()
	{

		int iter = 0; int NBcfc = 0;
		int[] num = new int[sommets.size() + 1];  Arrays.fill(num,0);  num[0] = sommets.size();
		int[] ro = new int[sommets.size() + 1];  Arrays.fill(ro,0);  ro[0] = sommets.size();
		int[] cfc = new int[sommets.size() + 1];  Arrays.fill(cfc,0);  cfc[0] = sommets.size();
		boolean[] entarj = new boolean[sommets.size() + 1];
		Vector pilch = new Vector();

		for(int s = 0; s < sommets.size(); s++)
		{
			if(num[s] == 0)
			{
				fortconnexe(s, iter, num, ro, entarj, pilch, NBcfc, cfc);
			}
		}

		Graphe g = new Graphe(this.nom, this.oriente, this.value);

		for(int i = 1; i < NBcfc; ++i)
		{ g.addSommet("cfc"+i); }

		for(Sommet s: sommets)
		{
			int ns = s.getKey();
			ArrayList<Integer> successeurs = struct.getSuccesseurs(ns);
		
			for(int suc: successeurs)
			{
				g.struct.addSuccesseur(cfc[ns],cfc[suc],struct.getArc(ns,suc));
			}
		}

		return g;

	}
	
	public ArrayList<Integer > pruffer(){
		
	Matrice m = (Matrice)this.struct;
    ArrayList<Integer> pruffer = new ArrayList<Integer>(this.nbSommets-2);
    //pruffer.add();

    //remplit le tableau avec le nombre d'arcs 
    int [] nbArcs= new int [this.nbSommets];
    Arrays.fill(nbArcs, 0);
    for(int i=0;i<this.nbSommets;i++) {
        for(int j=0;j<this.nbSommets;j++) {
            if(m.getMatrice()[i][j]!=Double.POSITIVE_INFINITY) {
                nbArcs[i]++;
                nbArcs[j]++;
            }
        }
    }

    int nbSommetsTraites=0;
    while(nbSommetsTraites!=this.nbSommets-2) {
        int i;
        for(i=0;i<this.nbSommets && nbArcs[i]!=1;i++) {}
        if(i<this.nbSommets && nbArcs[i]==1) {
            nbArcs[i]--;
            
            //chercher le sommet arrivee de l'arc
            int j=0;
            while(m.getArc(i,j)==Double.POSITIVE_INFINITY && m.getArc(j,i)==Double.POSITIVE_INFINITY) {//normalement le premier test �a sert � r
                j++;
            }
            pruffer.add(i);
            nbArcs[j]--;
            m.deleteSuccesseur(i, j);
            m.deleteSuccesseur(j, i);
            
        }
        nbSommetsTraites++;
    }
    return pruffer;
	
	}
}
