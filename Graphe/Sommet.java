package Graphe;

public class Sommet {
	
	private int key;
	private String nom;
	
	public Sommet(int key, String nom) {
		this.key=key;
		this.nom=nom;
	}

	@Override
	public String toString() {
		return key + " - " + nom;
	}
	public String getNom() {
		return this.nom;
	}

	public int getKey() {
		return key;
	}

	public void setKey(int key) {
		this.key = key;
	}
	
	

}
